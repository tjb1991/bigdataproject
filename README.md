# DeepNet README #
Primary Usage: BigDataProject.jar DeepNet, the rest of them are just test modules

Usage: java(hadoop) -jar BigDataProject.jar [module] (options...)

Implemented Modules:

     TestModule - Used to test the BigDataProject's command features.

     HadoopTest - Test out features of Hadoop, run this in Hadoop.

       DL4JTest - Test features of DL4J

        DeepNet - Train and test a neural network built using Hadoop.

### Jar ###

You'll need to export the Jar yourself to run on a server.


### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact