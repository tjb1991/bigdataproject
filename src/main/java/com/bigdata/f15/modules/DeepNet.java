package com.bigdata.f15.modules;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

import org.apache.log4j.BasicConfigurator;
import org.deeplearning4j.datasets.iterator.impl.MnistDataSetIterator;
import org.deeplearning4j.eval.Evaluation;
import org.deeplearning4j.nn.api.OptimizationAlgorithm;
import org.deeplearning4j.nn.conf.MultiLayerConfiguration;
import org.deeplearning4j.nn.conf.NeuralNetConfiguration;
import org.deeplearning4j.nn.conf.layers.ConvolutionLayer;
import org.deeplearning4j.nn.conf.layers.OutputLayer;
import org.deeplearning4j.nn.conf.layers.SubsamplingLayer;
import org.deeplearning4j.nn.conf.layers.setup.ConvolutionLayerSetup;
import org.deeplearning4j.nn.conf.preprocessor.CnnToFeedForwardPreProcessor;
import org.deeplearning4j.nn.conf.preprocessor.FeedForwardToCnnPreProcessor;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.nn.weights.WeightInit;
import org.deeplearning4j.optimize.api.IterationListener;
import org.deeplearning4j.optimize.listeners.ScoreIterationListener;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.dataset.SplitTestAndTrain;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;
import org.nd4j.linalg.factory.Nd4j;
import org.nd4j.linalg.lossfunctions.LossFunctions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.Log4jConfigurer;

import com.bigdata.f15.App;
import com.bigdata.f15.modules.interfaces.Module;
import com.bigdata.f15.support.ArgumentHandler;
import com.sun.tools.javac.util.List;

import lombok.extern.log4j.Log4j;

public class DeepNet implements Module{
	String[] args;
	Integer iterations = null;
	Integer numLayers = null;
	boolean useSpark = false;
	
	
	int seed = 123;
    int numSamples = 2000;
    int batchSize = 500;
    int nChannels = 1;
    int outputNum = 10;
	
	
	private static final Logger log = LoggerFactory.getLogger(DeepNet.class);
	
	@Override
	public String init(String[] parameters) {
		args = parameters;
		
		//Required arguments
		//How many iterations are we training the network for?
		iterations = ArgumentHandler.Handle(parameters).Argument("-i").getIntValue();
		if(iterations == null){
			return ArgumentHandler.getLastError();
		}
		//Layers? We'll need to create separate methods to do this
		numLayers = ArgumentHandler.Handle(parameters).Argument("-l").getIntValue();
		if(numLayers == null){
			return ArgumentHandler.getLastError();
		}
		if(numLayers<3 || numLayers>5){
			return "Invalid layer configuration.\n"
					+ "Valid configurations:\n"
					+ "3\t-Input | Middle (6 outputs) | Output\n"
					+ "4\t-Input | Middle (6 outputs) | Middle (6 outputs) | Output\n"
					+ "5\t-Input | Middle (6 outputs) | Middle (6 outputs) | Middle (6 outputs) | Output\n";
		}			
		
		return null;
	}

	@Override
	public String start() throws IOException {
		int numRows = 28;
        int numColumns = 28;
        int iterations = this.iterations;
        int splitTrainNum = (int) (batchSize*.8);
        
        int listenerFreq = iterations/5;
        DataSet mnist;
        SplitTestAndTrain trainTest;
        DataSet trainInput;
        ArrayList<INDArray> testInput = new ArrayList<>();
        ArrayList<INDArray> testLabels = new ArrayList<>();

        log.info("Load data....");
        DataSetIterator mnistIter = new MnistDataSetIterator(batchSize,numSamples, true);

        log.info("Build model....");
        
        MultiLayerConfiguration.Builder builder = null;
        
        if(numLayers == 3)
        	builder = threeLayerNetwork();
        else if(numLayers == 4)
        	builder = fourLayerNetwork();
        else if(numLayers == 5)
        	builder = fiveLayerNetwork();
        else{
        	return "This error should never occur...";
        }

        new ConvolutionLayerSetup(builder,numRows,numColumns,nChannels);

        MultiLayerConfiguration conf = builder.build();

        MultiLayerNetwork model = new MultiLayerNetwork(conf);
        model.init();

        log.info("Train model....");
        model.setListeners(Arrays.asList((IterationListener) new ScoreIterationListener(listenerFreq)));
        while(mnistIter.hasNext()) {
            mnist = mnistIter.next();
            trainTest = mnist.splitTestAndTrain(splitTrainNum, new Random(seed)); // train set that is the result
            trainInput = trainTest.getTrain(); // get feature matrix and labels for training
            testInput.add(trainTest.getTest().getFeatureMatrix());
            testLabels.add(trainTest.getTest().getLabels());
            model.fit(trainInput);
        }

        log.info("Evaluate weights....");

        log.info("Evaluate model....");
        Evaluation eval = new Evaluation(outputNum);
        for(int i = 0; i < testInput.size(); i++) {
            INDArray output = model.output(testInput.get(i));
            eval.eval(testLabels.get(i), output);
        }
        INDArray output = model.output(testInput.get(0));
        eval.eval(testLabels.get(0), output);
        log.info(eval.stats());
        log.info("****************Example finished********************");
		return "Finished";
		
	}
	

	
	private MultiLayerConfiguration.Builder threeLayerNetwork(){
		MultiLayerConfiguration.Builder builder = new NeuralNetConfiguration.Builder()
                .seed(seed)
                .batchSize(batchSize)
                .iterations(iterations)
                .constrainGradientToUnitNorm(true)
                .optimizationAlgo(OptimizationAlgorithm.STOCHASTIC_GRADIENT_DESCENT)
                .list(3)
                .layer(0, new ConvolutionLayer.Builder(10, 10)
                        .stride(2,2)
                        .nIn(nChannels)
                        .nOut(6)
                        .weightInit(WeightInit.XAVIER)
                        .activation("relu")
                        .build())
                .layer(1, new SubsamplingLayer.Builder(SubsamplingLayer.PoolingType.MAX, new int[] {2,2})
                        .build())
                .layer(2, new OutputLayer.Builder(LossFunctions.LossFunction.NEGATIVELOGLIKELIHOOD)
                        .nOut(outputNum)
                        .weightInit(WeightInit.XAVIER)
                        .activation("softmax")
                        .build())
                .backprop(true).pretrain(false);
		return builder;
	}
	
	private MultiLayerConfiguration.Builder fourLayerNetwork(){
		MultiLayerConfiguration.Builder builder = new NeuralNetConfiguration.Builder()
                .seed(seed)
                .batchSize(batchSize)
                .iterations(iterations)
                .constrainGradientToUnitNorm(true)
                .optimizationAlgo(OptimizationAlgorithm.STOCHASTIC_GRADIENT_DESCENT)
                .list(4)
                .layer(0, new ConvolutionLayer.Builder(10, 10)
                        .stride(2,2)
                        .nIn(nChannels)
                        .nOut(6)
                        .weightInit(WeightInit.XAVIER)
                        .activation("relu")
                        .build())
                .layer(1, new SubsamplingLayer.Builder(SubsamplingLayer.PoolingType.MAX, new int[] {2,2})
                        .build())
                .layer(2, new SubsamplingLayer.Builder(SubsamplingLayer.PoolingType.MAX, new int[] {2,2})
                        .build())
                .layer(3, new OutputLayer.Builder(LossFunctions.LossFunction.NEGATIVELOGLIKELIHOOD)
                        .nOut(outputNum)
                        .weightInit(WeightInit.XAVIER)
                        .activation("softmax")
                        .build())
                .backprop(true).pretrain(false);
		return builder;
	}
	
	private MultiLayerConfiguration.Builder fiveLayerNetwork(){
		MultiLayerConfiguration.Builder builder = new NeuralNetConfiguration.Builder()
                .seed(seed)
                .batchSize(batchSize)
                .iterations(iterations)
                .constrainGradientToUnitNorm(true)
                .optimizationAlgo(OptimizationAlgorithm.STOCHASTIC_GRADIENT_DESCENT)
                .list(5)
                .layer(0, new ConvolutionLayer.Builder(10, 10)
                        .stride(2,2)
                        .nIn(nChannels)
                        .nOut(6)
                        .weightInit(WeightInit.XAVIER)
                        .activation("relu")
                        .build())
                .layer(1, new SubsamplingLayer.Builder(SubsamplingLayer.PoolingType.MAX, new int[] {2,2})
                        .build())
                .layer(2, new SubsamplingLayer.Builder(SubsamplingLayer.PoolingType.MAX, new int[] {2,2})
                        .build())
                .layer(3, new SubsamplingLayer.Builder(SubsamplingLayer.PoolingType.MAX, new int[] {2,2})
                        .build())
                .layer(4, new OutputLayer.Builder(LossFunctions.LossFunction.NEGATIVELOGLIKELIHOOD)
                        .nOut(outputNum)
                        .weightInit(WeightInit.XAVIER)
                        .activation("softmax")
                        .build())
                .backprop(true).pretrain(false);
		return builder;
	}

	@Override
	public String getHelp() {
		String help = App.command + " DeepNet [function] (optionals)\n\n"
				+ "Required functions:\n"
				+ "-i ##\t- Number of iterations to train the network\n"
				+ "-l ##\t- Number of layers (min 3 max 5)\n"
				+ "\n"
				+ "Optional functions:\n"
				+ "-s \t- Use Spark for RAM or GPU speed";
		return help;
	}

}
