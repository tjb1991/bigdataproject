package com.bigdata.f15.modules.interfaces;

import java.io.IOException;

public interface Module {
	
	public String init(String[] parameters);
	
	public String start() throws Exception;
	
	public String getHelp();
	
}