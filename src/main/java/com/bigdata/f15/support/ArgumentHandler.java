package com.bigdata.f15.support;

public class ArgumentHandler {
	
	private String[] arguments;
	private int arglen;
	private String key;
	private int value = -1;
	private boolean argExists = false;
	public static String lastError = null;
	
	private ArgumentHandler(){}
	
	public static ArgumentHandler Handle(String[] arguments){
		return new ArgumentHandler(arguments);
	}
	
	public ArgumentHandler(String[] arguments){
		this.arguments = arguments;
		this.arglen = arguments.length;
	}
	
	public ArgumentHandler Argument(String key){
		this.key = key;
		if(arguments != null && arglen!=0){
			for(int i = 0; i < arglen; i++){
				if(arguments[i].equals(key)){
					value = i+1;
					argExists = true;
					break;
				}
			}
		}
		return this;
	}

	public Integer getIntValue(){
		Integer retVal = null;
		if(value > 0 && value < arglen){
			try{
				retVal = Integer.parseInt(arguments[value]);
			}
			catch(Exception e){
				ArgumentHandler.lastError = "Unable to handle input for key " + key + " | Value \""+ arguments[value] +"\"?";
				return null;
			}
			return retVal;
		}
		else{
			ArgumentHandler.lastError = "Not enough arguments. Looking for value of " + key;
			return null;
		}
		
	}
	
	public String getStringValue(){
		if(value > 0 && value < arglen){
			return arguments[value];
		}
		else{
			ArgumentHandler.lastError = "Not enough arguments.";
			return null;
		}
	}
	
	public boolean exists(){
		return argExists;
	}

	public static String getLastError(){
		String ret = ArgumentHandler.lastError;
		ArgumentHandler.lastError = null;
		return ret;
	}
}
