package com.bigdata.f15;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;

import com.bigdata.f15.modules.interfaces.Module;

public class App {
	
	public static String command = "java(hadoop) -jar BigDataProject.jar";
	
	private static String[][] modules = {
			{"TestModule", "Used to test the BigDataProject's command features."},
			{"HadoopTest", "Test out features of Hadoop, run this in Hadoop."},
			{"DL4JTest", "Test features of DL4J"},
			{"DeepNet", "Train and test a neural network built using Hadoop."},
	};
	
	public static void main(String[] args){
		
		if(args.length==0 || (args.length == 1 && args[0].equals("help"))){
			printHelp();
		}
		else{
			String moduleName = args[0];
			
			Class<?> myClass = null;
			Class<?>[] types = {};
			Constructor<?> constructor = null;
			Object[] parameters = {};
			Module instanceOfModule = null;
			
			try {
				myClass = Class.forName("com.bigdata.f15.modules." + moduleName);
			} catch (ClassNotFoundException e) {
				System.out.println("Module not found");
				printHelp();
				System.exit(0);
			}
			try {
				constructor = myClass.getConstructor(types);
			} catch (NoSuchMethodException | SecurityException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				instanceOfModule = (Module) constructor.newInstance(parameters);
			} catch (InstantiationException | IllegalAccessException | IllegalArgumentException
					| InvocationTargetException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			if(args.length == 2 && args[1].equals("-help")){
				System.out.println(instanceOfModule.getHelp());
			}
			
			String errorMessage = instanceOfModule.init(Arrays.copyOfRange(args, 1, args.length));
			
			if(errorMessage == null){
				try{
					instanceOfModule.start();
				}
				catch(Exception e){
					System.out.println(e);
				}
			}
			else{
				System.out.println(args[0] + " returned the following error:\n" + errorMessage);
				System.out.println();
				System.out.println(instanceOfModule.getHelp());
			}	
		}
	}
	
	public static void printHelp(){
		System.out.println("Usage: " + command + " [module] (options...)");
		System.out.println();
		System.out.println("Implemented Modules:");
		
		for(String[] module: modules){
			System.out.printf("%15s %s\n", module[0], "- " + module[1]);
		}
	}
}